<?php
session_start();
// error_reporting(E_ALL);
error_reporting(E_ERROR);
// ini_set('display_errors', 1);

include_once '../src/Model/Factory/dbFactory.php';
include_once '../src/Model/Entity/Utilisateur.php';

function isAuthenticated(): bool
{
    return (isset($_SESSION["Authenticated"]) && $_SESSION["Authenticated"] == 1);
}

function getDroits(): string
{
    $droits = "visiteur";
    if (isAuthenticated()) {
        $droits = $_SESSION["Droits"];
    }
    return $droits;
}

function echoSafe($str) {
  echo htmlspecialchars($str);
}
