<?php
include_once "../src/Utils/autoloader.php";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>VocasIItE | Problème</title>
	<link rel="icon" type="image/png" href="/img/logo.png">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/lib/bulma.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>
	<?php include_once '../src/View/navbar.php'; ?>
	<section class="section">
		<div class="container">
			<h3 id="title" class="title is-3">Signaler un problème</h3>
      <article class="message is-warning">
        <div class="message-header">
          <p>Il n'y a aucun problème sur ce site</p>
        </div>
        <div class="message-body">
          Enfin, il y en a peut-être (mais vraiment, c'est peu probable), et à ce moment je suis désolé de vous décevoir : il n'y a pas de formulaire pour en signaler un pour l'instant. Eh oui, c'est pas super utile finalement.
          <br>Dans tous les cas, vous pouvez contacter <strong>Pifra#1107</strong> sur Discord ou bien <strong>@Lefra99M</strong> sur Twitter pour en signaler si vous en voyez (on sait jamais, après tout).
        </div>
      </article>
		</div>
	</section>
</body>

</html>
