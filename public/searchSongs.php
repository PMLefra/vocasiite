<?php
  if (empty($_GET)) {
      exit;
  }

  header('Content-Type: application/json');

  include_once "../src/Utils/autoloader.php";
  $dbAdapter = (new dbFactory())->createService();
  $droits = getDroits();

  if ($droits == "visiteur") {
    echo "[]";
    exit;
  }

  $title = $_GET["title"] ?? "";
  $artist = $_GET["artist"] ?? "";

  $sql = <<<SQL
    SELECT nom, artiste, id, id_utilisateur FROM chanson
    WHERE UPPER(nom) LIKE UPPER(:title) AND UPPER(artiste) LIKE UPPER(:artist)
SQL;
  $result = $dbAdapter->prepare($sql);
  $result->bindValue(':title', '%'.$title.'%', PDO::PARAM_STR);
  $result->bindValue(':artist', '%'.$artist.'%', PDO::PARAM_STR);
  $result->execute();

  $songList = [];
  foreach ($result->fetchAll() as $row) {
    $songList[] = [];
    $lastIndex = array_key_last($songList);

    $songList[$lastIndex]["authorized"] = (($droits == "membre" && $_SESSION["Id"] == $row["id_utilisateur"]) || $droits == "admin");
    foreach ($row as $key => $value) {
      if (!is_numeric($key)) {
        if (is_string($value)) {
          $songList[$lastIndex][$key] = htmlspecialchars($value);
        } else {
          $songList[$lastIndex][$key] = $value;
        }
      }
    }
  }

  echo json_encode($songList);
