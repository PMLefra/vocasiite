<?php
  if (empty($_GET)) {
      exit;
  }

  header('Content-Type: application/json');

  include_once "../src/Utils/autoloader.php";
  $dbAdapter = (new dbFactory())->createService();
  $droits = getDroits();

  if ($droits == "visiteur") {
    echo "[]";
    exit;
  }

  $year1 = $_GET["year1"] ?? "";
  $year2 = $_GET["year2"] ?? "";

  $canSeePrivate = ($droits == "membre" || $droits == "admin");

  $sql = <<<SQL
    SELECT nom, theme, date_soiree, publique, id FROM soiree
    WHERE date_soiree BETWEEN ? AND ?
    ORDER BY date_soiree DESC
SQL;
  $result = $dbAdapter->prepare($sql);
  $result->bindValue(1, $year1.'-08-01', PDO::PARAM_STR);
  $result->bindValue(2, $year2.'-07-31', PDO::PARAM_STR);
  $result->execute();

  $soireeList = [];
  foreach ($result as $row) {
    if ($row["publique"] ||$canSeePrivate) {
      $soireeList[] = [];
      $lastIndex = array_key_last($soireeList);

      foreach ($row as $key => $value) {
        if (!is_numeric($key)) {
          if (is_string($value)) {
            $soireeList[$lastIndex][$key] = htmlspecialchars($value);
          } else {
            $soireeList[$lastIndex][$key] = $value;
          }
        }
      }
    }
  }

  echo json_encode($soireeList);
