<?php
require_once "../src/Utils/autoloader.php";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>VocasIItE | Contact</title>
  <link rel="icon" type="image/png" href="/img/logo.png">
  <link rel="stylesheet" href="/css/lib/bulma.css">
  <link rel="stylesheet" href="/css/main.css">
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>
  <?php require_once '../src/View/navbar.php'; ?>
  <section class="section">
    <div class="container">
      <h3 id="title" class="title is-3">Qui est derrière VocasIItE, et autres informations légales</h3>
      <article class="message">
        <div class="message-header">
          <p>Créateurs du site web</p>
        </div>
        <ul class="list is-hoverable">
          <li class="list-item">
            <div class="level">
              <span class="level-left">
                <strong class="level-left">Pierre-Marie "PM" Lefrançois</strong>&nbsp;<em>(responsable de publication)</em>
              </span>
              <div class="level-right">
                <a class="icon level-item" href="mailto:pm@lefra.net">
                  <i class="fas fa-envelope"></i>
                </a>
              </div>
            </div>
          </li>
          <li class="list-item">
            <span>François "Fluff" Lefoulon</span>
          </li>
        </ul>
      </article>

      <article class="message">
        <div class="message-header">
          <p>Sans oublier</p>
        </div>
        <div class="message-body" style="padding-top: 0.8em; padding-bottom: 0.8em;">
          <p>Merci à <em>Amaury Carrade</em>, <em>Valentin "Krokro" Dubromer</em> et <em>Rathea "Rathea" Uth</em> pour leur aide !</p>
        </div>
      </article>

      <article class="message">
        <div class="message-header">
          <p>Hébergement</p>
        </div>
        <div class="message-body" style="padding-top: 0.8em; padding-bottom: 0.8em;">
          <p>VocasIItE (site web et base de données) est hébergé chez <a href="http://www.alwaysdata.com">alwaysdata.</a></p>
          <p>Adresse : ALWAYSDATA, SARL, 91 rue du Faubourg Saint Honoré - 75008 Paris</p>
        </div>
      </article>

      <article class="message">
        <div class="message-header">
          <p>Données</p>
        </div>
        <div class="message-body" style="padding-top: 0.8em; padding-bottom: 0.8em; text-align: justify;">
          <p>Sera désigné par <em>VocalIIsE</em> le club du même nom de l'<a href="http://bde.iiens.net">AEIIE</a>.</p>
          <p>Les données conservées dans la base de données pour chaque utilisateur se connectant un jour à VocasIItE sont leur prénom, nom de famille, pseudonyme sur <a href="http://www.iiens.net">iiens.net</a>, identifiant AriseID et rôle au sein de <em>VocalIIsE</em>.</p>
          <p>Ces données sont conservées pour une durée indéterminée et ne son utilisées que pour le bon fonctionnement de VocasIItE :</p>
          <p>L'identifiant AriseID permet d'indentifier un compte utilisateur de manière unique dès qu'il doit en être fait référence dans la base de données (créateurs d'une chanson ou participation à une chanson lors d'une soirée par exemple).</p>
          <p>Le prénom, le pseudo et le nom de famille permettent d'afficher le nom complet d'un utilisateur sur le site web dès que cela est pertinent.</p>
          <p>Le rôle au sein de <em>VocalIIsE</em> permet de gérer les autorisations d'accès (lecture / écriture) à certaines parties et fonctionnalités de VocasIItE.</p>
        </div>
      </article>
    </div>
  </section>
</body>

</html>
