<?php
include_once "../src/Utils/autoloader.php";
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>VocasIItE | Placeholder</title>
	<link rel="icon" type="image/png" href="/img/logo.png">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/lib/bulma.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>
	<?php include_once '../src/View/navbar.php'; ?>
	<section class="section">
		<div class="container">
			<h3 id="title" class="title is-3">Ceci est une page placeholder</h3>
			<p>Si vous êtes ici, c'est que la page n'a pas été créée. Cette
	        page sert aussi de base si vous voulez créer une nouvelle page.</p>
		</div>
	</section>

</body>

</html>
