<?php
require_once("../src/OAuth2/ariseid/client/OAuthAriseClient.php");
require_once("../keys/keys.php");

$consumer = OAuthAriseClient::getInstance(
    $consumer_key,
    $consumer_secret,
    $consumer_private_key
);

$consumer->logout();
include_once("logout.php");
