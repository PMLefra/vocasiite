<?php
  if (empty($_GET)) {
      exit;
  }

  include_once("../src/Utils/autoloader.php");
  $dbAdapter = (new dbFactory())->createService();

  $id = $_GET["id"] ?? 0;
  $droits = getDroits();

  if ($droits != "membre" && $droits != "admin") {
    header("Location: /denied.php");
    exit();
  }

  $sql = "SELECT id_utilisateur FROM chanson WHERE id = :id";
  $result = $dbAdapter->prepare($sql);
  $result->bindValue(':id', $id, PDO::PARAM_INT);
  $result->execute();
  $isAuthor = $result->fetch()["id_utilisateur"] == $_SESSION["Id"];

  if ($id != 0 && (getDroits() == "admin" || $isAuthor)) {
      // Deux requêtes à la suite (j'ai essayé de faire les deux en une, ça ne marchait pas)
      $sql = "DELETE FROM lien WHERE id_chanson = :id";
      $result = $dbAdapter->prepare($sql);
      $result->bindValue(':id', $id, PDO::PARAM_INT);
      $result->execute();

      $sql = "DELETE FROM chanson WHERE id = :id";
      $result = $dbAdapter->prepare($sql);
      $result->bindValue(':id', $id, PDO::PARAM_INT);
      $result->execute();

      header('HTTP/1.1 204 No Content');
      exit();
      
  } else {
    header("Location: /denied.php");
    exit();
  }
